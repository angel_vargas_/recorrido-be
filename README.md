# Recorrido CL

This project is the backend part of an interview application when I was
applying to recorrido.cl

# Installation

You'll need:

- Ruby
- PostgreSQL
- Redis

```shell
bundle install
bundle exec rake db:create db:migrate
```

Import cities from recorrido.cl

```shell
bundle exec rake import:cities
```

# Running the app

```shell
bundle exec rails server
```

# Running the specs

```shell
bundle exec rspec
```

# Tasks

Create tickets dynamically

```shell
bundle exec rake create:tickets
```