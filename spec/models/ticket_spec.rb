require 'rails_helper'

RSpec.describe Ticket, type: :model do
  it { should belong_to(:origin).class_name('City') }
  it { should belong_to(:destination).class_name('City') }

  context 'callbacks' do
    it 'broadcasts the creation of the ticket' do
      expect { FactoryBot.create(:ticket) }
        .to have_broadcasted_to('ticket').with(hash_including(action: 'created'))
    end

    it 'broadcasts the statistics of the ticket' do
      expect { FactoryBot.create(:ticket) }
        .to have_broadcasted_to('ticket').with(hash_including(action: 'statistics'))
    end
  end
end
