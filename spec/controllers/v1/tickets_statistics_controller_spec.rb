
require 'rails_helper'

RSpec.describe V1::TicketsStatisticsController, type: :controller do
  describe '#index' do
    before do
      FactoryBot.create(:ticket, price: 5000)
      FactoryBot.create(:ticket, price: 2000)
    end

    it 'returns the ticket statistics' do
      get :index
      expect(JSON.parse(response.body)).to eq({
        'count' => 2,
        'income' => 7000
      })
    end

    it 'ignores the statistics from days before today' do
      Timecop.freeze(1.day.ago) do
        FactoryBot.create(:ticket, price: 3000)
      end

      get :index
      expect(JSON.parse(response.body)).to eq({
        'count' => 2,
        'income' => 7000
      })
    end
  end
end
