require 'rails_helper'

RSpec.describe V1::TicketsController, type: :controller do
  describe '#create' do
    it 'creates a new ticket' do
      origin = FactoryBot.create(:city)
      destination = FactoryBot.create(:city)
      post :create, {
        params: {
          ticket: {
            origin_id: origin.id,
            destination_id: destination.id,
            price: 5000
          }
        }
      }
      expect(Ticket.count).to eq(1)
      created_ticket = Ticket.last
      expect(created_ticket.origin).to eq(origin)
      expect(created_ticket.destination).to eq(destination)
      expect(created_ticket.price).to eq(5000)
    end
  end
end
