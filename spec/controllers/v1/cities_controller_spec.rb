require 'rails_helper'

RSpec.describe V1::CitiesController, type: :controller do
  describe '#index' do
    it 'assigns the cities' do
      city = FactoryBot.create(:city)
      get :index
      expect(assigns(:cities)).to eq([city])
    end
  end
end
