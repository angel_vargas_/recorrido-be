FactoryBot.define do
  factory :ticket do
    association :origin, factory: :city
    association :destination, factory: :city
  end
end