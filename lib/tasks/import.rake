require 'open-uri'

namespace :import do
  desc 'Imports cities from recorrido'
  task cities: :environment do
    json = open('https://www.recorrido.cl/api/v2/es/cities.json?country=chile').read
    parsed_json = JSON.parse(json)

    parsed_json['cities'].each do |city|
      City.where(recorrido_id: city['id']).first_or_create(
        name: city['name'],
        latitude: city['latitude'],
        longitude: city['longitude']
      )
    end
  end
end