namespace :create do
  desc 'Create tickets'
  task tickets: :environment do
    cities = City.all

    loop do
      origin, destination = cities.shuffle.take(2)
      price = rand(2000..35000)
      Ticket.create(origin_id: origin.id, destination_id: destination.id, price: price)
      sleep(rand(1..5))
    end
  end
end