class V1::TicketsController < ApplicationController
  def create
    @ticket = Ticket.new(ticket_params)
    if @ticket.save
      render json: @ticket
    else
      render json: @ticket.errors
    end
  end

  private

  def ticket_params
    params.require(:ticket).permit(:origin_id, :destination_id, :price)
  end
end