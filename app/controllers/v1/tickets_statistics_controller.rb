class V1::TicketsStatisticsController < ApplicationController
  def index
    @statistics = TicketStatistics.new
    render json: @statistics
  end
end