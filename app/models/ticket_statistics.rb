class TicketStatistics
  include ActiveModel::Serialization

  def count
    ticket_scope.count
  end

  def income
    ticket_scope.sum(:price)
  end

  private

  def ticket_scope
    Ticket.where('created_at >= ?', Time.now.beginning_of_day)
  end
end