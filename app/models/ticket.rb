class Ticket < ApplicationRecord
  belongs_to :origin, class_name: 'City'
  belongs_to :destination, class_name: 'City'

  after_commit :broadcast_creation, on: [:create]
  after_commit :broadcast_statistics, on: [:create]

  private

  def broadcast_creation
    ActionCable.server.broadcast 'ticket', {
      action: 'created',
      record: TicketSerializer.new(self)
    }
  end

  def broadcast_statistics
    ticket_statistics = TicketStatistics.new
    ActionCable.server.broadcast 'ticket', {
      action: 'statistics',
      record: TicketStatisticsSerializer.new(ticket_statistics)
    }
  end
end
