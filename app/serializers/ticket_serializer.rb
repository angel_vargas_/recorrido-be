class TicketSerializer < ActiveModel::Serializer
  attributes :id,
    :price

  belongs_to :origin
  belongs_to :destination
end