class TicketStatisticsSerializer < ActiveModel::Serializer
  attributes :count,
    :income
end