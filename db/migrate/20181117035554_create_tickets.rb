class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.integer :origin_id
      t.integer :destination_id
      t.integer :price

      t.timestamps
    end

    add_foreign_key :tickets, :cities, column: :origin_id
    add_foreign_key :tickets, :cities, column: :destination_id
  end
end
