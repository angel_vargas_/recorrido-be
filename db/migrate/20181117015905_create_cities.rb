class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.string :latitude
      t.string :longitude
      t.integer :recorrido_id

      t.timestamps
    end
  end
end
