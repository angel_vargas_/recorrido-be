Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  namespace :v1 do
    resources :cities, only: %i[index]
    resources :tickets_statistics, only: %i[index]
    resources :tickets, only: %i[create]
  end
end
